package pl.sienicki.onveloRecTask.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import pl.sienicki.onveloRecTask.model.Email;
import pl.sienicki.onveloRecTask.service.EmailService;

public class EmailControllerTest {

    private EmailController emailController;

    @Mock
    private EmailService emailServiceMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        emailController = new EmailController(emailServiceMock);
    }

    @Test
    public void getEmailReturnsEmailIfExists() {
        // Given
        Long id = 1L;
        Email email = new Email(id, "john.doe@example.com");
        when(emailServiceMock.getUser(id)).thenReturn(Optional.of(email));

        // When
        ResponseEntity<Email> response = emailController.getEmail(id);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(email);
    }

    @Test
    public void getEmailReturnsNotFoundIfEmailDoesNotExist() {
        // Given
        Long id = 1L;
        when(emailServiceMock.getUser(id)).thenReturn(Optional.empty());

        // When
        ResponseEntity<Email> response = emailController.getEmail(id);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void addEmailReturnsCreated() {
        // Given
        Email email = new Email(1L, "john.doe@example.com");

        // When
        ResponseEntity<String> response = emailController.addEmail(email);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo("Email john.doe@example.com created");
    }

    @Test
    public void updateEmailReturnsEmailIfExists() {
        // Given
        Long id = 1L;
        Email email = new Email(id, "john.doe@example.com");
        when(emailServiceMock.existsEmailById(id)).thenReturn(true);

        // When
        ResponseEntity<Email> response = emailController.updateEmail(id, email);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEqualTo(email);
    }

    @Test
    public void updateEmailReturnsNotFoundIfEmailDoesNotExist() {
        // Given
        Long id = 1L;
        Email email = new Email(id, "john.doe@example.com");
        when(emailServiceMock.existsEmailById(id)).thenReturn(false);

        // When
        ResponseEntity<Email> response = emailController.updateEmail(id, email);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void deleteEmailReturnsNoContentIfEmailExists() {
        // Given
        Long id = 1L;
        when(emailServiceMock.existsEmailById(id)).thenReturn(true);

        // When
        ResponseEntity<Email> response = emailController.deleteEmail(id);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }
}