package pl.sienicki.onveloRecTask.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import pl.sienicki.onveloRecTask.model.Email;
import pl.sienicki.onveloRecTask.model.EmailRequest;
import pl.sienicki.onveloRecTask.service.EmailSenderService;
import pl.sienicki.onveloRecTask.service.EmailService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EmailSenderControllerTest {

        @Mock
        private EmailService emailService;

        @Mock
        private EmailSenderService emailSenderService;

        @Test
        public void sendEmailToAllUsersTest() {
            // Given
            List<Email> users = new ArrayList<>();
            users.add(new Email("test1@example.com"));
            users.add(new Email("test2@example.com"));
            when(emailService.getEmails()).thenReturn(users);

            EmailRequest emailRequest = new EmailRequest("Test Subject", "Test Content");

            // When
            EmailSenderController emailSenderController = new EmailSenderController(emailService, emailSenderService);
            ResponseEntity<String> responseEntity = emailSenderController.sendEmailToAllUsers(emailRequest);

            // Then
            verify(emailService, times(1)).getEmails();
            verify(emailSenderService, times(1)).sendEmail(emailRequest.getSubject(), emailRequest.getContent(), List.of("test1@example.com", "test2@example.com"));

            assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            assertEquals("Email sent successfully", responseEntity.getBody());
        }
}