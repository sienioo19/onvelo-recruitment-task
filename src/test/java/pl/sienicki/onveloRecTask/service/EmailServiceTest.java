package pl.sienicki.onveloRecTask.service;

import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import pl.sienicki.onveloRecTask.model.Email;
import pl.sienicki.onveloRecTask.repositories.EmailRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class EmailServiceTest {
    @Mock
    private EmailRepository emailRepository;

    private EmailService emailService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        emailService = new EmailService(emailRepository);
    }

    @Test
    @DisplayName("Test saveEmail method")
    public void testSaveEmail() {
        Email email = new Email("example@example.com");
        emailService.saveEmail(email);
        verify(emailRepository, times(1)).save(email);
    }

    @Test
    @DisplayName("Test deleteEmail method with existing id")
    public void testDeleteEmailWithExistingId() {
        Long emailId = 1L;
        when(emailRepository.existsById(emailId)).thenReturn(true);
        emailService.deleteEmail(emailId);
        verify(emailRepository, times(1)).deleteById(emailId);
    }

    @Test
    @DisplayName("Test getUser method with existing id")
    public void testGetUserWithExistingId() {
        Long emailId = 1L;
        Email email = new Email("example@example.com");
        when(emailRepository.findById(emailId)).thenReturn(Optional.of(email));
        Optional<Email> result = emailService.getUser(emailId);
        assertEquals(email, result.get());
    }

    @Test
    @DisplayName("Test getUser method with non-existing id")
    public void testGetUserWithNonExistingId() {
        Long emailId = 1L;
        when(emailRepository.findById(emailId)).thenReturn(Optional.empty());
        Optional<Email> result = emailService.getUser(emailId);
        assertTrue(result.isEmpty());
    }

    @Test
    @DisplayName("Test editEmail method with existing id")
    public void testEditEmailWithExistingId() {
        Long emailId = 1L;
        Email email = new Email("example@example.com");
        Email existingEmail = new Email("old@example.com");
        existingEmail.setId(emailId);
        when(emailRepository.findById(emailId)).thenReturn(Optional.of(existingEmail));
        emailService.editEmail(email, emailId);
        verify(emailRepository, times(1)).save(existingEmail);
        assertEquals(email.getEmail(), existingEmail.getEmail());
    }
}