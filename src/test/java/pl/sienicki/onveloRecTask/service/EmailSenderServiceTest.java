package pl.sienicki.onveloRecTask.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
@ExtendWith(MockitoExtension.class)
public class EmailSenderServiceTest {

    @Mock
    private JavaMailSender javaMailSender;

    @InjectMocks
    private EmailSenderService emailSenderService;

    @Captor
    ArgumentCaptor<SimpleMailMessage> messageCaptor;

    @Test
    public void sendEmail_shouldSendEmailWithCorrectSubjectAndContent() {

        String subject = "Test Subject";
        String content = "Test Content";
        List<String> recipients = Arrays.asList("recipient1@example.com", "recipient2@example.com");

        emailSenderService.sendEmail(subject, content, recipients);
        verify(javaMailSender).send(messageCaptor.capture());

        SimpleMailMessage emailRequest = messageCaptor.getValue();
        assertThat(emailRequest.getSubject()).isEqualTo(subject);
        assertThat(emailRequest.getText()).isEqualTo(content);
        assertThat(emailRequest.getTo()).containsExactlyInAnyOrderElementsOf(recipients);
    }

}