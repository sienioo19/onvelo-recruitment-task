package pl.sienicki.onveloRecTask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnveloRecTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnveloRecTaskApplication.class, args);
	}

}
