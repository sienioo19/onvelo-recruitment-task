package pl.sienicki.onveloRecTask.service;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sienicki.onveloRecTask.model.Email;
import pl.sienicki.onveloRecTask.repositories.EmailRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmailService {
    private final EmailRepository emailRepository;

    public void saveEmail(Email user){
        emailRepository.save(user);
    }

    public void deleteEmail(Long id){
        emailRepository.deleteById(id);
    }

    public Optional<Email> getUser(Long id){
        return emailRepository.findById(id);
    }
     public void editEmail(Email user, Long id){
         Email userToEdit = emailRepository.findById(id)
                 .orElseThrow(() -> new EntityNotFoundException("Email with id: " + id + "doesn't exist"));
         userToEdit.setId(id);
         userToEdit.setEmail(user.getEmail());
         emailRepository.save(userToEdit);
     }
     public boolean existsEmailById(Long id){
        return emailRepository.existsById(id);
     }

    public List<Email> getEmails() {
        return emailRepository.findAll();
    }
}
