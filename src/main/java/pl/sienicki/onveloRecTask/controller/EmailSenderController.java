package pl.sienicki.onveloRecTask.controller;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.sienicki.onveloRecTask.model.EmailRequest;
import pl.sienicki.onveloRecTask.model.Email;
import pl.sienicki.onveloRecTask.service.EmailSenderService;
import pl.sienicki.onveloRecTask.service.EmailService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/emailSender")
public class EmailSenderController {
    private final EmailService emailService;
    private final EmailSenderService emailSenderService;

    private static final Logger INTEGRATION_LOGGER = LoggerFactory.getLogger("org.springframework.web");
    @PostMapping("/send")
    public ResponseEntity<String> sendEmailToAllUsers(@RequestBody EmailRequest request){
        List<Email> users = emailService.getEmails();
        List<String> recipients = users.stream()
                .map(Email::getEmail)
                .collect(Collectors.toList());
        emailSenderService.sendEmail(request.getSubject(), request.getContent(), recipients);
        INTEGRATION_LOGGER.info("Received request to /emailSender/send with Subject: " + request.getSubject() + " and content: "+request.getContent() + " recipients are:  " +  recipients + ", " );
        return ResponseEntity.ok("Email sent successfully");
    }
}
