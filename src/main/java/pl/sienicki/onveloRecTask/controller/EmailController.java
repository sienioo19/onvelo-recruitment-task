package pl.sienicki.onveloRecTask.controller;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sienicki.onveloRecTask.model.Email;
import pl.sienicki.onveloRecTask.service.EmailService;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/emails")
public class EmailController {
    private final EmailService emailService;

    private static final Logger INTEGRATION_LOGGER = LoggerFactory.getLogger("org.springframework.web");

    @GetMapping("/{id}")
    public ResponseEntity<Email> getEmail(@PathVariable Long id) {
        Optional<Email> email = emailService.getUser(id);
        INTEGRATION_LOGGER.info("Received request to /emails/ " + id);
        return email.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<String> addEmail(@RequestBody Email email) {
        emailService.saveEmail(email);
        INTEGRATION_LOGGER.info("Received request to /emails/ creating email: " + email.getEmail() );
        return ResponseEntity.ok("Email " + email.getEmail() + " created");

    }

    @PutMapping("/{id}")
    public ResponseEntity<Email> updateEmail(@PathVariable Long id, @RequestBody Email email) {
        if (!emailService.existsEmailById(id)) {
            return ResponseEntity.notFound().build();
        }
        emailService.editEmail(email, id);
        INTEGRATION_LOGGER.info("Received request to /emails/" + id + " updating email with id: "+ id +" to email:" + email.getEmail() );
        email.setId(id);
        return ResponseEntity.ok(email);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Email> deleteEmail(@PathVariable Long id) {
        if(!emailService.existsEmailById(id)){
            return ResponseEntity.notFound().build();
        }
        emailService.deleteEmail(id);
        INTEGRATION_LOGGER.info("Received request to /emails/" + id + " deleting email with id: " + id );
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/all")
    public List<Email> getAllEmails() {
        INTEGRATION_LOGGER.info("Received request to /emails/all and got list off all emails ind DB");
        return emailService.getEmails();
    }
}
