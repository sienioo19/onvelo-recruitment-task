# Mikroserwis wysyłający maile do użytkowników (Onvelo Recruitment Task)
Ten projekt to mikroserwis napisany w języku Java z wykorzystaniem frameworka Spring Boot oraz narzędzia Maven. Serwis jest odpowiedzialny za przetrzymywanie adresów email użytkowników oraz umożliwia wysyłkę wiadomości email do wszystkich użytkowników znajdujących się w bazie danych.

## Funkcjonalności

**Serwis posiada następujące funkcjonalności:**

* Przechowywanie adresów email użytkowników
* Operacje CRUD na adresach email użytkowników poprzez REST API
* Wysyłanie wiadomości email do wszystkich użytkowników z bazy danych poprzez REST API
* Generowanie dwóch plików logujących: log integracyjny i log ogólny

## Instalacja i uruchamianie
Aby zainstalować i uruchomić serwis, wykonaj następujące kroki:

**Sklonuj repozytorium:**

git clone https://gitlab.com/sienioo19/onvelo-recruitment-task.git 

**Przejdź do katalogu z projektem:**

cd onvelo-recruitment-task

**Uruchom aplikację:**

mvn spring-boot:run

**Serwis będzie dostępny pod adresem** http://localhost:8080.

**Do wysyłki emaili niezbędne jest podanie credentaili w pliku aplication.properties**
  1. **spring.mail.username=xxx**       tutaj zamiast xxx podajemy nasz email (aplikacja skonfigurowana jest pod poczte na hostingu gmail.com)
  2. **spring.mail.password=xxx** tutaj zamias xxx podajemy nasze hasło do powyższego email-a (zgodnie z polityką bezpieczenśtwa gmail musimy w ustawieniach bezpieczenstwa wygenerować hasło do aplikacji, następnie to hasło wklejamy tutaj)

## Pliki logów
**Serwis tworzy dwa pliki logujące w katalogu logs (tworzonym automatycznie):**

* general.log - ogólny log aplikacji
* integration.log - log integracyjny, logujący przychodzące requesty